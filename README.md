# 3DCityDB

A dockerized [3DCityDB](https://www.3dcitydb.org/) using
[PostGIS](https://postgis.net/).

## Usage

To start a new 3DCityDB, just run:

```
docker run -d --name mycitydb -p 5432:5432 registry.gitlab.com/geo-bl-ch/docker/3dcitydb
```

Using the default configuration, you can access the database with the following
connection string: `postgresql://postgres:password@<your-host>:5432/citydb`

> **NOTE:** The database initialization process may take some seconds. So you
cannot connect immediately after the container is ready.

Please refer to the [PostGIS image](https://gitlab.com/geo-bl-ch/docker/postgis) for further
information about the configuration options.

Additionally, the container can be customized using the following environment variables:

**`EPSG`**
        EPSG code to be used (Default: 2056).

**`HEIGHT_EPSG`**
        EPSG code to be used for heights (Default: 0).

**`SKIP_INIT`**
        Skip initialization of 3DCityDB (Default: false).
